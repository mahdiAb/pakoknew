import React from 'react';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Main from "./components/pages/services/Main";
import Login from "./components/pages/auth/login/Login";
import UiKit from "./components/microComponents/UiKit/UiKit";



const App = () => {
  return (
      <BrowserRouter>
        <Routes>
          <Route path="/business/:id" element={<Main/>}/>
            <Route path="/login" element={<Login/>}/>

          <Route path='/UiKit' element={<UiKit/>}/>
          {/*<Route path='/' element={<BusinessList/>}/>*/}

          {/*<Route path="/register" element={<Register/>}/>*/}
          {/*<Route path="/branchManagement" element={<BranchManagement/>}/>*/}
          {/*<Route path="/buyAndSell/goodsServices" element={<GoodsServices/>}/>*/}
          {/*<Route path="/buyAndSell/goodsServices/commodity" element={<Commodity/>}/>*/}
          {/*<Route path="/buyAndSell/goodsServices/services" element={<Services/>}/>*/}
          {/*<Route path="/buyAndSell/Invoicing/buy" element={<Buy/>}/>*/}
          {/*<Route path="/buyAndSell/Invoicing/returnSale" element={<ReturnSale/>}/>*/}
          {/*<Route path="/buyAndSell/Invoicing/returnShopping" element={<ReturnShopping/>}/>*/}
          {/*<Route path="/buyAndSell/Invoicing/sale" element={<Sale/>}/>*/}
          {/*<Route path="/changePass" element={<ChangePass/>}/>*/}
          {/*<Route path="/createBusiness" element={<CreateBusiness/>}/>*/}
          {/*<Route path="/editBusiness" element={<EditBusiness/>}/>*/}
          {/*<Route path="/ExtensionBusiness/:id" element={<ExtensionBusiness/>}/>*/}
          {/*<Route path="/order/:id" element={<Order/>}/>*/}
          {/*<Route path="/upgradeBusiness/:id" element={<UpgradeBusiness/>}/>*/}
          {/*<Route path="/userManagement" element={<UserManagement/>}/>*/}

        </Routes>
      </BrowserRouter>
  );
};

export default App;