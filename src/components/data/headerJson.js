import React from 'react';

import {GrMoney} from "@react-icons/all-files/gr/GrMoney";
import {HiOutlineUsers} from "@react-icons/all-files/hi/HiOutlineUsers";
import {BsReverseLayoutTextWindowReverse} from "@react-icons/all-files/bs/BsReverseLayoutTextWindowReverse";
import {GiMoneyStack} from "@react-icons/all-files/gi/GiMoneyStack";
import Installment from "../pages/services/installment/Installment";
import Acc from "../pages/services/acc/Acc";
import Invoice from "../pages/services/invoice/Invoice";
import Setting from "../pages/services/setting/Setting";


export const tabsData = [
    {
        id: 0,
        text: 'ٌصورت حساب',
        icon: <GrMoney size={20}/>,
        content: <Invoice/>,
    },
    {
        id: 0,
        text: 'اقساط',
        icon: <GrMoney size={20}/>,
        content: <Installment/>,
    },
    {
        id: 1,
        text: 'حسابداری',
        icon: <GiMoneyStack size={20}/>,
        content: <Acc/>,

    },
    {
        id: 2,
        text: 'مدیریت محتوا',
        icon: <BsReverseLayoutTextWindowReverse size={20}/>,
        content: 'Find ssstab content',
    },
    {
        id: 3,
        text: 'نوبتدهی',
        icon: <HiOutlineUsers size={20}/>,
        content: 'Find tab content',
    },
    {
        id: 3,
        text: 'تنظیمات',
        icon: <HiOutlineUsers size={20}/>,
        content: <Setting/>,
    },


];


