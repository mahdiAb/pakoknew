const Domain = 'http://gateway.pakok.ir/api/'


const oauthApiService = Domain + 'oauth/v1/';
const Login = oauthApiService + 'user/Login'
const GetBusinessToken = oauthApiService + 'token/GetBusinessToken'


const businessService = Domain + 'business/v1/';
const PersonsService = Domain + 'Persons/v1/Person/';
const CatalogService = Domain + 'Catalog/v1/Catalog/';


const Logout = oauthApiService + 'user/Logout'
const GenerateSMSOTP = oauthApiService + 'otp/GenerateSMSOTP'
const ChangeForgottenPassword = oauthApiService + 'user/ChangeForgottenPassword'
const RegisterUser = oauthApiService + 'user/RegisterUser'
const ChangePassword = oauthApiService + 'user/ChangePassword'
const TestBusinessToken = oauthApiService + 'token/TestBusinessToken'
const GetServiceListByBusinessTypeID = businessService + 'service/GetServiceListByBusinessTypeID'
const CalculatedCostSelectedService = businessService + 'service/CalculatedCostSelectedService'
const GetBusinessTypeList = businessService + 'Business/GetBusinessTypeList'
const GetBusinessByBusinessIDForExtension = businessService + 'Business/GetBusinessByBusinessIDForExtension'
const GetBusinessByBusinessIDForUpgrade = businessService + 'Business/GetBusinessByBusinessIDForUpgrade'
const GetBusinessList = businessService + 'Business/GetBusinessList'
const CreateBusiness = businessService + 'Business/CreateBusiness'
const AddOrderExtensionBusiness = businessService + 'order/AddOrderExtensionBusiness'
const GetOrderByBusinessId = businessService + 'order/GetOrderByBusinessId'
const ConfirmOrder = businessService + 'order/ConfirmOrder'
const DeleteOrder = businessService + 'order/DeleteOrder'


const CreatePerson = PersonsService + 'CreatePerson'
const GetAllPerson = PersonsService + 'GetAllPerson'
const UpdatePerson = PersonsService + 'UpdatePerson'
const RemovePerson = PersonsService + 'RemovePerson'



const CreateCatalog = CatalogService + 'CreateCatalog'
const UpdateCatalog = CatalogService + 'UpdateCatalog'
const DeleteCatalog = CatalogService + 'DeleteCatalog'
const GetAllCatalog = CatalogService + 'GetAllCatalog'
const GetCatalogByID = CatalogService + 'GetCatalogByID'


export {
    Login, GetBusinessToken,
    GetServiceListByBusinessTypeID, GenerateSMSOTP, ChangeForgottenPassword, ChangePassword, RegisterUser,
    CreateBusiness, CreateCatalog, DeleteCatalog, GetCatalogByID, UpdateCatalog, GetAllCatalog,
    GetOrderByBusinessId, GetAllPerson,
    ConfirmOrder, TestBusinessToken,RemovePerson,UpdatePerson,
    GetBusinessByBusinessIDForExtension,
    GetBusinessTypeList,
    GetBusinessList,
    CalculatedCostSelectedService, Logout,
    DeleteOrder, CreatePerson,
    AddOrderExtensionBusiness,
    GetBusinessByBusinessIDForUpgrade,

}
