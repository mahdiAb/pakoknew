import CreateRequest from "./CreateRequest";

import Cookies from 'js-cookie'
import {Login} from "../apiList/apiListBusiness";


class Request extends CreateRequest {
    constructor(url, callbackData, callbackEr) {
        super();
        this.callbackData = callbackData
        this.callbackEr = callbackEr
        this.mainUrl = url
        this.login = false
        this.mainMethod = 'post'
        this.mainParams = null
        this.mainData = null
        this.mainHeader = null
        this.router = null
        this.query = null
        this.token = Cookies.get('Token');
        this.refreshToken = Cookies.get('RefreshToken')

    }

    static init(url, callbackData, callbackEr) {

        return new Request(url, callbackData, callbackEr);
    }


    setMainUrl(value) {
        this.mainUrl = value
        this.url = value
        return this;
    }


    setMainMethod(value) {
        this.mainMethod = value
        this.method = value
        return this;
    }

    setMainParams(value) {
        this.mainParams = value
        this.params = value
        return this;
    }

    setMainData(value) {
        this.mainData = value
        this.data = value
        return this;
    }

    loginPages() {
        this.login = true
        return this;
    }

    setMainHeader(value) {
        this.header = value
        this.mainHeader = value
        return this;
    }

    setToken(value) {
        this.token = value
        this.setMyHeader('Authorization', `Bearer ${value}`)
        return this;
    }

    setRouter(value) {
        this.router = value
        return this;
    }


    handleRemoveAllCookies() {

        Cookies.remove('Token')
        Cookies.remove('RefreshToken')

        if (window.location.pathname !== '/Login' && window.location.pathname !== '/register') {
            this.router(`/Login`)
        }


    }

    async afterGetNewToken(response) {

        await Cookies.set(`Token`, response.data.jwtToken, { expires: 7 })
        await Cookies.set(`RefreshToken`, response.data.refreshToken, { expires: 7 })
        await this.setToken(Cookies.get('Token'))
          this.refreshToken = response.data.refreshToken
        await this.setMainToken(response.data.jwtToken)
        await this.setMyHeader('Authorization', `Bearer ${response.data.jwtToken}`)
        await this.repeatRequest()


    }


    async getNewUserRefreshToken() {

        let data = {'RefreshToken': this.refreshToken}
        await this.setMyHeader('Authorization', `Bearer ${this.token}`).setUrl(Login).setData(data).handleRequest(async (data, status) => {
            // this.setData(null)

            if (status === 'response') {
                await this.afterGetNewToken(data)
            } else {
                await this.handleRemoveAllCookies()
            }
        })

    }


    async repeatRequest() {
        await this.setMainUrl(this.mainUrl).setMyHeader('Authorization', `Bearer ${this.mainToken}`).setMainToken(this.mainToken).callRequest()
    }

    async callRequest() {

        this.url = this.mainUrl

        await this.setMyHeader('Authorization', `Bearer ${this.token}`)

        await this.handleRequest(async (data, status) => {
            if (status === 'response') {
                this.callbackData(data)
            } else {
                if (data.response.status === 401 && !this.login) {
                    await this.getNewUserRefreshToken()

                } else {
                    await this.callbackEr(data)
                }
            }
        })

    }
}

export default Request