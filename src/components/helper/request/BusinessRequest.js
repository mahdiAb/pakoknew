import CreateRequest from "./CreateRequest";
import Cookies from 'js-cookie'
import {GetBusinessToken, Login} from "../apiList/apiListBusiness";


class BusinessRequest extends CreateRequest {
    constructor(url, callbackData, callbackEr) {
        super();
        this.callbackData = callbackData
        this.callbackEr = callbackEr
        this.mainUrl = url
        this.login = false
        this.mainMethod = 'post'
        this.mainParams = null
        this.mainData = null
        this.mainHeader = null
        this.router = null
        this.query = null
        this.token = Cookies.get('Token');
        this.mainToken = Cookies.get('Token');
        this.refreshToken = Cookies.get('RefreshToken')
        this.BToken = null
        this.BRefreshToken = null
        this.BID = null

    }

    static init(url, callbackData, callbackEr) {

        return new BusinessRequest(url, callbackData, callbackEr);
    }


    setMainUrl(value) {
        this.mainUrl = value
        this.url = value
        return this;
    }

    setMainToken(value) {
        this.mainToken = value
        this.setToken(value)
        return this;
    }

    setMainMethod(value) {
        this.mainMethod = value
        this.method = value
        return this;
    }

    setMainParams(value) {
        this.mainParams = value
        this.params = value
        return this;
    }

    setMainData(value) {
        this.mainData = value
        this.data = value
        return this;
    }

    loginPages() {
        this.login = true
        return this;
    }

    setMainHeader(value) {
        this.header = value
        this.mainHeader = value
        return this;
    }

    setToken(value) {
        this.token = value
        this.setMyHeader('Authorization', `Bearer ${value}`)
        return this;
    }

    setRouter(value) {
        this.router = value
        return this;
    }


    setBToken(BID) {

        this.BToken = Cookies.get(`BusinessToken_${BID}`)
        return this;
    }

    setBRefreshToken(BID) {

        this.BRefreshToken = Cookies.get(`BusinessRefreshToken_${BID}`)
        return this;
    }

    setBID(value) {
        this.BID = value
        this.setBRefreshToken(value)
        this.setBToken(value)
        return this;
    }


    handleRemoveAllCookies() {

        Cookies.remove('Token')
        Cookies.remove('RefreshToken')
        Cookies.remove(`BusinessRefreshToken_${this.BID}`)
        Cookies.remove(`BusinessToken_${this.BID}`)

        if (window.location.pathname !== '/Login' && window.location.pathname !== '/register') {
            this.router(`/Login`)
        }


    }

    async afterGetNewUserToken(response) {

        await Cookies.set(`Token`, response.data.jwtToken)
        await Cookies.set(`RefreshToken`, response.data.refreshToken)
        await this.setToken(Cookies.get('Token'))
        this.refreshToken = response.data.refreshToken


        await this.getNewBusinessRefreshTokenByID()


    }

    async getNewBusinessRefreshTokenByID() {

        let data = {
            BusinessID: this.BID
        }
        await this.setMyHeader('Authorization', `Bearer ${this.token}`).setUrl(GetBusinessToken).setData(data).handleRequest(async (data, status) => {
            this.setData(null)
            if (status === 'response') {
                await this.afterGetNewBusinessToken(data)
            } else {
                await this.handleRemoveAllCookies()
            }
        })
    }

    async getNewUserRefreshToken() {
        console.log('getNewUserRefreshToken')
        let data = {'RefreshToken': this.refreshToken}
        await this.setMyHeader('Authorization', `Bearer ${this.token}`).setUrl(Login).setData(data).handleRequest(async (data, status) => {
            // this.setData(null)

            if (status === 'response') {
                await this.afterGetNewUserToken(data)
            } else {
                await this.handleRemoveAllCookies()
            }
        })

    }

    async afterGetNewBusinessToken(response) {

        Cookies.set(`BusinessRefreshToken_${this.BID}`, response.data.refreshToken)
        Cookies.set(`BusinessToken_${this.BID}`, response.data.jwtToken)


        this.url = this.mainUrl
        await this.setMyHeader('Authorization', `Bearer ${response.data.jwtToken}`).handleRequest(async (data, status) => {

            if (status === 'response') {
                this.callbackData(data)
            } else {
                if (data.response.status == 401) {
                    await this.getNewBusinessRefreshToken()
                }
            }
        })


    }

    async getNewBusinessRefreshToken() {

        let GetBTokenData = {'RefreshToken': this.BRefreshToken}
        await this.setMyHeader('Authorization', `Bearer ${this.token}`).setUrl(GetBusinessToken).setData(GetBTokenData).handleRequest(async (data, status) => {
            // this.setData(null)
            if (status === 'response') {
                if (data.data.jwtToken) {
                    await this.afterGetNewBusinessToken(data)
                } else {
                    await this.getNewBusinessRefreshTokenByID()
                }

            } else {

                await this.getNewUserRefreshToken()
            }
        })


    }

    async repeatRequest() {
        await this.setMainUrl(this.mainUrl).setMyHeader('Authorization', `Bearer ${this.mainToken}`).setMainToken(this.mainToken).callRequest()
    }

    async callRequest() {

        this.url = this.mainUrl

        await this.setMyHeader('Authorization', `Bearer ${this.BToken}`)

        await this.handleRequest(async (data, status) => {
            if (status === 'response') {
                this.callbackData(data)
            } else {
                if (data.response.status == 401 && !this.login) {
                    await this.getNewBusinessRefreshToken()

                } else {
                    await this.callbackEr(data)
                }
            }
        })

    }
}

export default BusinessRequest