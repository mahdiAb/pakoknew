import axios from "axios";

class CreateRequest {
    constructor() {
        this.url = null
        this.method = 'post'
        this.params = null
        this.data = null
        this.header = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',

        }


    }

    setUrl(newValue) {
        this.url = newValue
        return this;
    }

    setMethod(newValue) {
        this.method = newValue
        return this;
    }

    setParams(newValue) {
        this.params = newValue
        return this;
    }

    setData(newValue) {
        this.data = newValue
        return this;
    }


    setMyHeader(key, value) {
        this.header[key] = value
        return this
    }


    async handleRequest(callback) {

        await axios({
            method: this.method, url: this.url, data: this.data, headers: this.header, params: this.params,
        })
            .then(async (response) => {

                await callback(response, 'response')

            })
            .catch(async (error) => {

                await callback(error, 'error')


            })


        return this
    }


}

export default CreateRequest;