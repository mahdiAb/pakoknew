import * as Yup from 'yup';


export const validation = {
    mobile: (name) => Yup.string().required(`لطفا ${name}  را وارد کنید. `).matches(/^(\+98|0)?9\d{9}$/, `  ${name} صحیح نیست! `),
    string: (name) => Yup.string().required(`لطفا ${name}  را وارد کنید. `),
    otp: (name) => Yup.string().matches(/^[0-9]{6}$/, `  ${name} صحیح نیست! `).required(`لطفا ${name}  را وارد کنید. `),
    number10: (name) => Yup.string().matches(/^[0-9]{10}$/, `  ${name} صحیح نیست! `).required(`لطفا ${name}  را وارد کنید. `),
    number12: (name) => Yup.string().matches(/^[0-9]{12}$/, `  ${name} صحیح نیست! `).required(`لطفا ${name}  را وارد کنید. `),
    checkbox: (message) => Yup.boolean().oneOf([true], message),
    number: (name) => Yup.number().required(`لطفا ${name}  را وارد کنید. `),

}


export const toEnDigit = (e) => {
    return e.target.value.replace(/[\u0660-\u0669\u06f0-\u06f9]/g,
        function (a) {
            return a.charCodeAt(0) & 0xf
        }
    )

}