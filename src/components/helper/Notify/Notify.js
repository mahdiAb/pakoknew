import Notify from 'devextreme/ui/notify';

// const types = ['error', 'info', 'success', 'warning'];
function show(type = 'error', message = 'مشکلی پیش امده لطفا دوباره تلاش کنید!') {
    const position = 'top right';
    const direction = 'down-push';

    Notify({
        message: message,
        type: type,
        width: 300,
        displayTime: 3500,
        animation: {
            show: {
                type: 'fade', duration: 400, from: 0, to: 1,
            },
            hide: {type: 'fade', duration: 40, to: 0},
        },
    }, {

        position,
        direction,
    });

}


export default show


