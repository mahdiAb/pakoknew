import React from 'react';
import {useFormik,Formik} from 'formik';


import * as Yup from 'yup';
import Input from "../common/uikits/Input";

const initialValues = {
    name: '', email: '', pass: '', number: ''
}

const handleBlur = (values) => {
    console.log(values)
}


const validationSchema = Yup.object().shape({
    name: Yup.string().min(7, 'name length invalid').required('name in required'), // email: Yup.string().required('jjjjj').email('invalid email'),
    // pass: Yup.string().required('jjjjj'),
    // number: Yup.string().required('number').matches(/^[0-9]{11}$/, 'invalid phone number'),

})


const UiKit = () => {
    const onSubmit = (values) => {
        console.log(values)
    }
    const formik = useFormik({
        initialValues, onSubmit, handleBlur, validationSchema, validateOnMount: true, enableReinitialize: true
    })


    return (<div>


        <form onSubmit={formik.handleSubmit}>
            <label htmlFor="email">Email Address</label>

            <Input formik={formik} name={'name'} placeholder={'MyInput'}/>

            <button type="submit" disabled={!formik.isValid}>Submit</button>
        </form>


        {/*       <form onSubmit={formik.handleSubmit}>*/}
        {/*           <input className='border' name="name" type="text"   {...formik.getFieldProps('name')}/>*/}

        {/*           {formik.errors.name && formik.touched.name && (*/}
        {/*               <div className='font-blue text-blue'>{formik.errors.name}</div>)}*/}

        {/*<input className='border' name="number" type="text"   {...formik.getFieldProps('number')}/>*/}


        {/*           {formik.errors.number && formik.touched.number && (*/}
        {/*               <div className='font-blue text-blue'>{formik.errors.number}</div>)}*/}


        {/*           <input className='border' name="email" type="email" onChange={formik.handleChange}*/}
        {/*                  value={formik.values.email} onBlur={formik.handleBlur}/>*/}


        {/*           {formik.errors.email && formik.touched.email && (*/}
        {/*               <div className='font-blue text-blue'>{formik.errors.email}</div>)}*/}


        {/*           <input className='border' name="pass" type="password" onChange={formik.handleChange}*/}
        {/*                  value={formik.values.pass}/>*/}
        {/*           <button type="submit" disabled={!formik.isValid}>Submit</button>*/}
        {/*       </form>*/}

        <Formik
            initialValues={{ name: "" }}
            onSubmit={() => {}}
            validationSchema={validationSchema}
        >
            {(props) => {
                const { handleBlur, setFieldValue, values, errors, touched } = props;
                return (
                    <form onSubmit={props.handleSubmit}>
                        <h1>Email Form</h1>

                        <input
                            type="email"
                            name="email"
                            onChange={(e)=>{
                                const value = e.target.value || "";
                                setFieldValue('email', value.toLowerCase());
                            }}
                            onBlur={handleBlur}
                            value={values.email}
                        />
                        {errors.email && touched.email && errors.email}

                        <button type="submit">Submit</button>

                     </form>
                );
            }}
        </Formik>
    </div>);
};

export default UiKit;