import React from 'react';

const Input = ({name, formik, placeholder }) => {


    return (<div class='m-2'>

        <div class="input_wrap">
            <textarea
                className={'defInput'}


                {...formik.getFieldProps(name)}
            />


            <label className={formik.values[name] !== "" && 'active'}>{placeholder}</label>
        </div>
        {formik.errors[name] && formik.touched[name] && (
            <div className='font-blue text-red text-sm'>{formik.errors[name]}</div>)}

    </div>);
};
export default Input;


