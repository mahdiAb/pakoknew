import React from 'react';
import {Select} from "antd";

const SearchSelect = ({name, formik, fieldNames, placeholder, displayExpr = "Name", options}) => {


    return (<div class='m-2'>

        <div class={'input_wrap'}>

            <Select
                className={'defInput'}
                formik={formik}
                name={name}
                onChange={(e) => {
                    formik.setFieldValue(name, e);
                }}
                onBlur={formik.handleBlur}
                value={formik.values[name]}
                showSearch
                fieldNames={fieldNames}
                optionFilterProp="children"
                filterOption={(input, option) => (option?.label ?? '').includes(input)}
                filterSort={(optionA, optionB) => (optionA?.label ?? '').toLowerCase().localeCompare((optionB?.label ?? '').toLowerCase())}
                options={options}
            />
            <label className={formik.values[name] !== "" && 'active'}>{placeholder}</label>
        </div>
        {formik.errors[name] && formik.touched[name] && (
            <div className='font-blue text-red text-sm'>{formik.errors[name]}</div>)}

    </div>);
};
export default SearchSelect;

