import React from 'react';
import {toEnDigit} from "../../../helper/validation/Validation";

const Input = ({name, formik,className, placeholder, type = 'text'}) => {


    return (<div class='m-2'>

            <div  className={`input_wrap ${className}`}>
                <input
                    className={'defInput'}
                    name={name}
                    type={type}
                    onChange={(e) => {
                        formik.setFieldValue(name, toEnDigit(e));
                    }}
                    onBlur={formik.handleBlur}
                    value={formik.values[name]}
                />


                <label className={formik.values[name] !== "" && 'active'}>{placeholder}</label>
            </div>
            {formik.errors[name] && formik.touched[name] && (
                <div className='font-blue text-red text-sm'>{formik.errors[name]}</div>)}

    </div>);
};
export default Input;


//
// name????
// onBlur
// onChange
// value
// {...formik.getFieldProps(name)}