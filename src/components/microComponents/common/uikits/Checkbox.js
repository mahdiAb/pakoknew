import React from 'react';

const Checkbox = ({name, formik, placeholder}) => {

    console.log(formik.errors[name])
    console.log(formik.values[name])
    return (<div class='m-9'>


        <input


            type="checkbox"
            {...formik.getFieldProps(name)}
        />


        <label>{placeholder}</label>
        {formik.errors[name] && formik.touched[name] && (
            <div className='font-blue text-red text-sm'>{formik.errors[name]}</div>)}


    </div>);
};
export default Checkbox;


