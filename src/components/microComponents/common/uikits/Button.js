import React from 'react';
import Loading from "../loading/Loading";

const Button = ({children, loading , type = 'button', disabled, onClick}) => {


    return (
        <button  className={`p-2 bg-blue flex items-center rounded-xl text-white  ${disabled && 'cursor-not-allowed'} ${loading && 'cursor-progress'}`} disabled={disabled || loading} type={type} onClick={onClick}>

            {loading && <Loading  />}

            {children}
        </button>
    );
};

export default Button;