import React, {useState} from 'react';
import momentJalaali from 'moment-jalaali';
import DatePicker from "react-datepicker2";









const   DateBox= ({name, formik, placeholder }) => {
    let  [value,setValue]=useState(momentJalaali('1396/7/6', 'jYYYY/jM/jD'))


    return (<div class='m-2'>

        <div class="input_wrap" onBlur={()=>console.log(999)}>

            <DatePicker
                className={'defInput'}
                name={name}

                onChange={(e) => {
                   setValue(e)
                    formik.setFieldValue(name, e);
                }}
                // onBlur={formik.handleBlur}
                value={value}


                persianDigits={false}


                isGregorian={false}
                timePicker={false}
            />

            <label className={'active'}>{placeholder}</label>
            {/*<label className={formik.values[name] !== "" && 'active'}>{placeholder}</label>*/}
        </div>
        {formik.errors[name] && formik.touched[name] && (
            <div className='font-blue text-red text-sm'>{formik.errors[name]}</div>)}

    </div>);
};

    export default DateBox;