import React from 'react';

const KeyValue = ({keyText, value, className, keyClass, valueClass}) => {
    return (

        <div className={`px-2 my-3 flex items-center  ${className}`}>
            <div className={`font-bold text-sm ml-1  ${keyClass}`}>{keyText + ': '}  </div>
            <div className={`text-sm ml-1  ${valueClass}`}>{value}</div>
        </div>
    );
};

export default KeyValue;