import React from 'react';
import SearchSelect from "../uikits/SearchSelect";
import TextBox from "../uikits/TextBox";

const simpleProducts = [{Name: 'HD Video Player', ID: 0}, {
    Name: 'SuperHD Video Player', ID: 1
}, {Name: 'SuperPlasma 50', ID: 2}, {Name: 'SuperLED 50', ID: 3}, {Name: 'SuperLED 42', ID: 4}, {
    Name: 'SuperLCD 55', ID: 5
}, {Name: 'SuperLCD 42', ID: 6}, {Name: 'SuperPlasma 65', ID: 7}, {Name: 'SuperLCD 70', ID: 8}, {
    Name: 'Projector Plus', ID: 9
}, {Name: 'Projector PlusHT', ID: 10}, {Name: 'ExcelRemote IR', ID: 11}, {
    Name: 'ExcelRemote Bluetooth', ID: 12
}, {Name: 'ExcelRemote IP', ID: 13},];


const GetAddress = ({itemClassName, formik}) => {


    return (<>
        <div className={itemClassName}>
            <SearchSelect name={'StateID'} formik={formik} placeholder={'استان'}


                          fieldNames={{label: 'Name', value: 'ID'}}

                          optionFilterProp="children"
                          filterOption={(input, option) => (option?.label ?? '').includes(input)}
                          filterSort={(optionA, optionB) => (optionA?.label ?? '').toLowerCase().localeCompare((optionB?.label ?? '').toLowerCase())}
                          options={simpleProducts}


            />
        </div>
        <div className={itemClassName}>


            <SearchSelect
                name={'CityID'} formik={formik} placeholder={'شهر'}
                fieldNames={{label: 'Name', value: 'ID'}}
                optionFilterProp="children"
                filterOption={(input, option) => (option?.label ?? '').includes(input)}
                filterSort={(optionA, optionB) => (optionA?.label ?? '').toLowerCase().localeCompare((optionB?.label ?? '').toLowerCase())}
                options={simpleProducts}
            />


        </div>
        <div className={itemClassName}>
            <TextBox name='Extra' formik={formik} placeholder={'آدرس کامل'}/>
        </div>


    </>);

}

export default GetAddress;
