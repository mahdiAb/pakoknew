import React from 'react';
import {Dropdown} from "antd";
import {HiUser} from "@react-icons/all-files/hi/HiUser";
import {FiUser} from "@react-icons/all-files/fi/FiUser";

const Layout = ({children}) => {

    const items = [
        {
            label: 'خروج',
            key: '0',
        },

    ];


    return (
        <div>
            <header className='w-full top-0 fixed p-4 shadow-md z-10 flex justify-between items-center'>


                <div>
                    <img src="./Logo.png" width={80} alt=""/>
                </div>


                <div>

                    <Dropdown overlayStyle={{width:'120px'}} arrow menu={{items}} trigger={['click']} >
                        <FiUser size={20}/>
                    </Dropdown>
                </div>
            </header >
            <div className="mt-14">
                {children}
            </div>
        </div>
    );
};

export default Layout;