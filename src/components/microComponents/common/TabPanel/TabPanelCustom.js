import React, {useState} from 'react';
import TabPanel from "devextreme-react/tab-panel";

const TabPanelCustom = ({tabs,className}) => {

    let [selectedIndex, setSelectedIndex] = useState(0)


    return (

            <div className="">
                <div className="flex items-center justify-center p-5 border-2 border-black-200 border-b-0 rounded-t-lg">
                    <img src="./Logo.png" width={130} alt=""/>
                </div>
                <TabPanel
                    rtlEnabled={true}
                    dataSource={tabs}
                    selectedIndex={selectedIndex}
                    onOptionChanged={(args) => {
                        if (args.name === 'selectedIndex') {
                            setSelectedIndex(args.value)
                        }
                    }}
                    className={className}
                    itemTitleRender={(data) => <span>{data.text}</span>}
                    itemComponent={(data) => data.data.content}
                    // animationEnabled={true}
                    // swipeEnabled={true}
                />

            </div>

    );
};

export default TabPanelCustom;