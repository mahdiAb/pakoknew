import React from 'react';
import {Menu, MenuItem, ProSidebarProvider, Sidebar, SubMenu} from 'react-pro-sidebar';

import {ImArrowLeft2} from "@react-icons/all-files/im/ImArrowLeft2";
import {ImArrowRight2} from "@react-icons/all-files/im/ImArrowRight2";
import {connect} from "react-redux";
import {handleCollapseSidebar} from "../redux/reducer/Reducer";

const SidebarBox = ({handleAddTab,tabs, collapseSidebar,tabListState, handleCollapseSidebar, items, handleNewTabIndex}) => {
    const handleCollapse = () => {
        handleCollapseSidebar()
    }


    const multi = (data) => {

        let item =

            <SubMenu label={data.title} icon={data.icon}>
                {data.children.map((i, index) => (<>{i.children?.length > 0 ? multi(i) : single(i)}</>))}


            </SubMenu>


        return item


    }


    const single = (data) => {
        let item = <MenuItem component={


            <button onClick={() =>     handleAddTab({
                title: data.title,
                content: data.content,
                id: handleNewTabIndex,
            })


            }


        > </button>
    }
>
    {
        data.title
    }
</MenuItem>


    return item


}
return (<div className='overflow-hidden'>
    <ProSidebarProvider>
        <div className='flex   h-[calc(100vh-40px)] text-sm'>
            <Sidebar width={'220px'} rtl collapsed={collapseSidebar} defaultCollapsed={collapseSidebar}
                     collapsedWidth={'55px'}
            >
                <div className="flex justify-between flex-col h-full">

                    <Menu>
                        {items.map((data, index) => (
                                data.children?.length > 0 ? multi(data) : single(data)
                            )
                        )}


                    </Menu>
                    <button
                        className={`bg-blue p-2 w-full text-white text-center flex justify-center font-bold text-xl `}
                        onClick={handleCollapse}>
                        {collapseSidebar ? <ImArrowLeft2/> : <ImArrowRight2/>}</button>
                </div>
            </Sidebar>
        </div>

    </ProSidebarProvider>

</div>);
}
;


function mapStateToProps(state) {
    return {
        collapseSidebar: state.collapseSidebar,
        tabListState: state.settingTabList,
    }
}


export default connect(mapStateToProps, {
    handleCollapseSidebar,

})(SidebarBox);