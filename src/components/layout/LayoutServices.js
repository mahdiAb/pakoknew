import React, {useCallback} from 'react';
import {Sortable} from 'devextreme-react/sortable';
import {TabPanel} from 'devextreme-react/tab-panel';

import SidebarBox from "./SidebarBox";
import {connect} from "react-redux";
import {handleCollapseSidebar} from "../redux/reducer/Reducer";


const LayoutServices = ({
                            collapseSidebar, handleCollapseSidebar,
                            tabs, handleTabList,
                            items,
                            handleNewTabIndex,
                            handleCloseTab,
                            handleAddTab,
                            activeKey,
                            handleActiveKey
                        }) => {


    const closeButtonHandler = useCallback((item) => {

        handleCloseTab(item)

    }, [handleActiveKey]);


    const renderTitle = useCallback((data) => (<>
        <div>
        <span>
          {data.title}
        </span>
            <i className="dx-icon dx-icon-close !ml-0 !mr-2" onClick={() => {
                closeButtonHandler(data);
            }}/>
        </div>
    </>), [closeButtonHandler]);
    const tabInner = useCallback((data) => (<>
        <div className='p-4'>
            {data.data.content}
        </div>
    </>), [closeButtonHandler]);

    const onSelectionChanged = useCallback((args) => {
        handleActiveKey(args.addedItems[0]);
    }, [handleActiveKey]);

    const onTabDragStart = useCallback((e) => {
        e.itemData = e.fromData[e.fromIndex];
    }, []);

    const onTabDrop = useCallback((e) => {
        const newTabs = [...tabs];

        newTabs.splice(e.fromIndex, 1);
        newTabs.splice(e.toIndex, 0, e.itemData);

        handleTabList(newTabs);
    }, [tabs]);


    return (<div className='servicesLayout flex'>
        <SidebarBox items={items} handleAddTab={handleAddTab} handleNewTabIndex={handleNewTabIndex}/>
        <div
            className={`transition-all ease-out duration-300 ${collapseSidebar ? 'w-[calc(100%-55px)]' : 'w-[calc(100%-220px)]'}`}>


            <Sortable
                filter=".dx-tab"
                data={tabs}
                itemOrientation="horizontal"
                dragDirection="horizontal"
                onDragStart={onTabDragStart}
                onReorder={onTabDrop}
            >
                <TabPanel
                    dataSource={tabs}

                    itemTitleRender={renderTitle}
                    deferRendering={false}
                    // animationEnabled={true}
                    showNavButtons={true}
                    selectedItem={tabs[activeKey]}
                    repaintChangesOnly={true}
                    onSelectionChanged={onSelectionChanged}
                    itemComponent={tabInner}
                />

            </Sortable>

        </div>


    </div>);
};

function mapStateToProps(state) {
    return {
        collapseSidebar: state.collapseSidebar,

    }
}


export default connect(mapStateToProps, {
    handleCollapseSidebar,

})(LayoutServices);


