import {createStore} from "redux";
import {GrMoney} from "@react-icons/all-files/gr/GrMoney";
import {ImTable} from "@react-icons/all-files/im/ImTable";

import React from 'react';
import AccTabs1 from "../../pages/services/acc/tabs/AccTabs1";
import InstallmentTabs1 from "../../pages/services/installment/tabs/InstallmentTabs1";

import Reducer from "../reducer/Reducer";
import CreateInvoice from "../../pages/services/invoice/tabs/CreateInvoice";
import PersonList from "../../pages/services/setting/Users/PersonList";

export default createStore(Reducer, {
    collapseSidebar: false,

    accTabList: [{title: 'داشبورد', icon: <GrMoney size={20}/>, content: <AccTabs1/>, id: 1}],
    accNewTabIndex: 1,
    accActiveKey: 0,
    accNewTab: null,


    installmentTabList: [
        {title: 'داشبورد', icon: <GrMoney size={20}/>, content: <InstallmentTabs1/>, id: 1},

    ],
    installmentNewTabIndex: 1,
    installmentActiveKey: 0,
    installmentNewTab: null,


    invoiceTabList: [
        {title: 'داشبورد', icon: <ImTable size={20}/>, content: <CreateInvoice/>, id: 1},

    ],
    invoiceNewTabIndex: 1,
    invoiceActiveKey: 0,
    invoiceNewTab: null,

    settingTabList: [
        {title: 'داشبورد', icon: <ImTable size={20}/>, content: <PersonList/>, id: 1},

    ],
    settingNewTabIndex: 1,
    settingActiveKey: 0,
    settingNewTab: null,


})



