const initialState = {
    accTabList: [],
    accNewTabIndex: 1,
    collapseSidebar: false,
    accActiveKey: 0,
    accNewTab: null,


    installmentTabList: [],
    installmentNewTabIndex: 1,
    installmentActiveKey: 0,
    installmentNewTab: null,


    invoiceTabList: [],
    invoiceNewTabIndex: 1,
    invoiceActiveKey: 0,
    invoiceNewTab: null,


    settingTabList: [],
    settingNewTabIndex: 1,
    settingActiveKey: 0,
    settingNewTab: null,
}


const Reducer = (state = initialState, action) => {
    switch (action.type) {

        case COLLAPSE_SIDEBAR:
            return {...state, collapseSidebar: !state.collapseSidebar}



        case ACC_TAB_LIST:
            return {...state, accTabList: action.payload}
        case ACC_ACTIVE_KEY:
            return {...state, accActiveKey: action.payload}
        case ACC_MULTIPLE:

            return {...state, accNewTabIndex: state.accNewTabIndex + 1}
        case ACC_NEW_TAB:

            return {...state, accNewTab: action.payload}




        case INSTALLMENT_TAB_LIST:
            return {...state, installmentTabList: action.payload}
        case INSTALLMENT_ACTIVE_KEY:
            return {...state, installmentActiveKey: action.payload}
        case INSTALLMENT_MULTIPLE:

            return {...state, installmentNewTabIndex: state.installmentNewTabIndex + 1}
        case INSTALLMENT_NEW_TAB:

            return {...state, installmentNewTab: action.payload}




        case INVOICE_TAB_LIST:
            return {...state, invoiceTabList: action.payload}
        case INVOICE_ACTIVE_KEY:
            return {...state, invoiceActiveKey: action.payload}
        case INVOICE_MULTIPLE:
            return {...state, invoiceNewTabIndex: state.invoiceNewTabIndex + 1}
        case INVOICE_NEW_TAB:
            return {...state, invoiceNewTab: action.payload}




        case SETTING_TAB_LIST:
            return {...state, settingTabList: action.payload}
        case SETTING_ACTIVE_KEY:
            return {...state, settingActiveKey: action.payload}
        case SETTING_MULTIPLE:
            return {...state, settingNewTabIndex: state.settingNewTabIndex + 1}
        case SETTING_NEW_TAB:
            return {...state, settingNewTab: action.payload}


        default:
            return state;
    }
}

export default Reducer;

/*****************action************/
const COLLAPSE_SIDEBAR = "handleCollapseSidebar"
const ACC_TAB_LIST = "handleAccTabList"
const ACC_MULTIPLE = "handleAccNewTabIndex"
const ACC_ACTIVE_KEY = "handleAccActiveKey"
const ACC_NEW_TAB = "handleAccNewTab"


const INSTALLMENT_TAB_LIST = "handleInstallmentTabList"
const INSTALLMENT_MULTIPLE = "handleInstallmentNewTabIndex"
const INSTALLMENT_ACTIVE_KEY = "handleInstallmentActiveKey"
const INSTALLMENT_NEW_TAB = "handleInstallmentNewTab"


const INVOICE_TAB_LIST = "handleInvoiceTabList"
const INVOICE_MULTIPLE = "handleInvoiceNewTabIndex"
const INVOICE_ACTIVE_KEY = "handleInvoiceActiveKey"
const INVOICE_NEW_TAB = "handleInvoiceNewTab"


const SETTING_TAB_LIST = "handleSettingTabList"
const SETTING_MULTIPLE = "handleSettingNewTabIndex"
const SETTING_ACTIVE_KEY = "handleSettingActiveKey"
const SETTING_NEW_TAB = "handleSettingNewTab"





export function handleCollapseSidebar() {
    return {
        type: COLLAPSE_SIDEBAR,

    }
}
export function handleInstallmentTabList(x) {
    return {
        type: INSTALLMENT_TAB_LIST,
        payload: x
    }
}


export function handleInstallmentNewTabIndex() {
    return {
        type: INSTALLMENT_MULTIPLE,

    }
}

export function handleInstallmentActiveKey(x) {
    console.log(x)
    return {
        type: INSTALLMENT_ACTIVE_KEY,
        payload: x
    }
}

export function handleInstallmentNewTab(x) {
    return {
        type: INSTALLMENT_NEW_TAB,
        payload: x

    }
}


export function handleAccTabList(x) {
    return {
        type: ACC_TAB_LIST,
        payload: x
    }
}


export function handleAccNewTabIndex() {
    return {
        type: ACC_MULTIPLE,

    }
}

export function handleAccActiveKey(x) {
    return {
        type: ACC_ACTIVE_KEY,
        payload: x
    }
}

export function handleAccNewTab(x) {
    return {
        type: ACC_NEW_TAB,
        payload: x

    }
}




export function handleInvoiceTabList(x) {
    return {
        type: INVOICE_TAB_LIST,
        payload: x
    }
}


export function handleInvoiceNewTabIndex() {
    return {
        type: INVOICE_MULTIPLE,

    }
}

export function handleInvoiceActiveKey(x) {
    return {
        type: INVOICE_ACTIVE_KEY,
        payload: x
    }
}

export function handleInvoiceNewTab(x) {
    return {
        type: INVOICE_NEW_TAB,
        payload: x

    }
}


// region Setting
export function handleSettingTabList(x) {
    return {
        type: SETTING_TAB_LIST,
        payload: x
    }
}


export function handleSettingNewTabIndex() {
    return {
        type: SETTING_MULTIPLE,

    }
}

export function handleSettingActiveKey(x) {
    return {
        type: SETTING_ACTIVE_KEY,
        payload: x
    }
}

export function handleSettingNewTab(x) {
    return {
        type: SETTING_NEW_TAB,
        payload: x

    }
}

//  region end