import {tabsData} from '../../data/headerJson.js';
import React, {useState} from 'react';


const Main = () => {


    let [selectedIndex, setSelectedIndex] = useState(0)



    return <div>
        <header>
             <div className='flex px-4 shadow-md'>
                <div className="flex justify-center items-center ml-5"><img width={100} src="/logo.png" alt="logo"/>
                </div>

                <ul className="tabs flex ">
                    {
                        tabsData.map((item, index) => (
                            <li className={`items-center   cursor-pointer flex tab mr-4  ${selectedIndex === index && 'border-b-2 border-b-blue'}`}
                                onClick={() => setSelectedIndex(index)}>
                                {item.icon}<span>{item.text}</span>
                            </li>

                        ))
                    }


                </ul>

            </div>
        </header>
        <div className="tab-panes  ">

            {
                tabsData.map((item, index) => (

                    <div
                        className={`tab-pane ${selectedIndex === index && 'active'}`}>{item.content}</div>


                ))
            }
        </div>

    </div>


}

export default Main;
