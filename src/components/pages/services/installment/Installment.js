import React, {useEffect} from 'react';
import LayoutServices from "../../../layout/LayoutServices";


import {connect} from "react-redux";
import {
    handleInstallmentActiveKey,
    handleInstallmentNewTabIndex,
    handleInstallmentTabList
} from "../../../redux/reducer/Reducer";
import {GiMoneyStack} from "@react-icons/all-files/gi/GiMoneyStack";



const Installment = ({
                         handleInstallmentTabList, handleInstallmentActiveKey, handleInstallmentNewTabIndex,
                         newTab, tabListState, activeKey, newTabIndex,
                     }) => {
    let items = [{

        title: '1111',
        icon: <GiMoneyStack size={20}/>,
        children: [
            {content: <p>jjj</p>, title: '22222'},
            {
                icon: <GiMoneyStack size={20}/>,
                title: '33333',
                children: [{content: <p>jjj</p>, title: '44444'}, {content: <p>jjj</p>, title: '5555'},]
            },]
    }]

    useEffect(() => {

        if (newTab) {
            add({content: newTab.content, title: newTab.tabTitle})
        }
    }, [newTab])



    const add = (data) => {
        handleInstallmentNewTabIndex()


        const newPanes = [...tabListState];
        newPanes.push({
            title: data.title, content: data.content, id: newTabIndex,
        });
        handleInstallmentTabList(newPanes)

        handleInstallmentActiveKey(newTabIndex);

    };

    const remove = (item) => {
        console.log(item)
        const newTabListState = [...tabListState];
        const index = newTabListState.indexOf(item);

        newTabListState.splice(index, 1);


        if (index >= newTabListState.length && index > 0) {
            handleInstallmentActiveKey(newTabListState[index - 1]);
        }

        handleInstallmentTabList(newTabListState)


    };


    const handleInstallmentActive = (x) => {
        handleInstallmentActiveKey(x)
    }
    return (


        <LayoutServices tabs={tabListState} items={items}
                        activeKey={activeKey}
                        handleNewTabIndex={handleInstallmentNewTabIndex}
                         handleCloseTab={remove}
                        handleAddTab={add}
                        handleActiveKey={handleInstallmentActive}
                        handleTabList={handleInstallmentTabList}
        />

    );
};


function mapStateToProps(state) {
    return {
        tabListState: state.installmentTabList,
        newTabIndex: state.installmentNewTabIndex,
        activeKey: state.installmentActiveKey,
        newTab: state.installmentNewTab,
    }
}


export default connect(mapStateToProps, {
    handleInstallmentTabList,
    handleInstallmentActiveKey,
    handleInstallmentNewTabIndex
})(Installment);

