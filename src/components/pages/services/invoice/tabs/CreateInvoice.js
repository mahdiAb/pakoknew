import React, {useCallback, useRef, useState} from 'react';
import DataGrid, {
    Button as DevButton,
    Column,
    ColumnFixing,Texts,
    Editing,
    Grouping,
    KeyboardNavigation,
    Paging, RequiredRule,
    RowDragging,
    Scrolling,
    StateStoring,
    Summary,
    TotalItem,
} from 'devextreme-react/data-grid';
import {data2, getMaxID} from "../../../../data/data";
import {Switch} from "devextreme-react/switch";
import Input from "../../../../microComponents/common/uikits/Input";
import {useNavigate, useParams} from "react-router-dom";
import BusinessRequest from "../../../../helper/request/BusinessRequest";
import {CreatePerson} from "../../../../helper/apiList/apiListBusiness";
import show from "../../../../helper/Notify/Notify";
import * as Yup from "yup";
import {validation} from "../../../../helper/validation/Validation";
import {useFormik} from "formik";
import DateBox from "../../../../microComponents/common/uikits/DateBox";
import Button from "../../../../microComponents/common/uikits/Button";
import {DropDownButton} from "devextreme-react";

let initialValues = {
    InvoiceDate: '',
    PersonID: '',
    InvoiceAddDate: '',
    InternalInvoiceNo: '',
    RefInvoiceID: '',
    InvoiceSubjectID: '',
    InvoiceSettlementMethodID: '',
    CashPaymentAmount: '',
    LoanPaymentAmount: '',
    NormalSaleInvoiceDetails: [
        // {
        //     ProductID: '',
        //     ProductDescription: '',
        //     Value: '',
        //     UnitAmount: '',
        //     DiscountAmount: '',
        // }
    ],

}
const validationSchema = Yup.object().shape({
    InvoiceDate: validation.string('شماره موبایل'),
    PersonID: validation.string('نام'),
    InvoiceAddDate: validation.string('نام خانوادگی'),
    InternalInvoiceNo: validation.number10('کد ملی'),
    RefInvoiceID: validation.number10('کد پستی'),
    InvoiceSubjectID: validation.number10('شماره ثبت'),
    InvoiceSettlementMethodID: validation.string('نوع کاربر'),
    CashPaymentAmount: validation.number12('شماره اقتصادی'),
    LoanPaymentAmount: validation.string('شماره اقتصادی'),




})
const CreateInvoice = ({pattern}) => {

    let {dataGridRef} = useRef();

    const [data, setData] = useState(data2);
    const navigate = useNavigate();
    let {id} = useParams();
    let [loading, setLoading] = useState(false)


    const onSaving = useCallback(async (e) => {
        console.log(e)
        console.log(formik.values)
        if (e.changes.length) {
            await e.component.refresh(true);
            e.component.cancelEditData();


        }
    }, []);
    console.log(dataGridRef)
    const handleDelete = (e) => {

        let list = [...data]
        list.splice(e.row.rowIndex, 1)

        setData(list)
    }


    const onReorder = (e) => {
        const visibleRows = e.component.getVisibleRows();
        const newTasks = [...data];

        const toIndex = newTasks.findIndex((item) => item.ID === visibleRows[e.toIndex].data.ID);
        const fromIndex = newTasks.findIndex((item) => item.ID === e.itemData.ID);

        newTasks.splice(fromIndex, 1);
        newTasks.splice(toIndex, 0, e.itemData);

        setData(newTasks)
    }

    const onFocusedCellChanged = (e) => {


        if (e.rowIndex === data.length - 1 && e.columnIndex !== 10 && e.columnIndex !== 0) {
        }
    }
    const onFocusedCellChanging = (e) => {
        // grid.instance.addRow();
        // grid.instance.deselectAll();
        e.isHighlighted = true;
        let list = [...data];


        if (e.newRowIndex === data.length - 1 && e.newColumnIndex !== 10 && e.newColumnIndex !== 0) {


            let x = e.rows[e.rows.length - 1].data
            let keys1 = Object.keys(x);
            let values1 = Object.values(x);

            let y = false

            for (let i = 0; i < keys1.length; i++) {

                if (keys1[i] !== 'ID' && values1[i] !== null) {

                    y = true
                }
            }

            if (y) {

                let data = {

                    ProductID: null,
                    ProductDescription: null,
                    Value: null,
                    UnitAmount: null,
                    DiscountAmount: null,
                }
                let clonedItem = {...data, ID: getMaxID()};
                list.push(clonedItem);
                setData(list)
            }


        }


    }


    const fullNameColumn_calculateCellValue = (rowInfo) => {

        return Number(rowInfo.inflection) + Number(rowInfo.number)
    }


    const handleCreatePerson = (values) => {

        setLoading(true)


        BusinessRequest.init(CreatePerson, function (data) {
            setLoading(false)
            if (data.data.messageID === 0) {
                show('success', 'کاربر با مفقیت ثبت شد')
            } else {
                show('error', data.data.message)

            }
        }, function (error) {
            setLoading(false)

        }).setBID(id).setRouter(navigate).setMainData(values).callRequest()
    }





    const formik = useFormik({
        initialValues: initialValues,
        onSubmit: values => {
            handleCreatePerson(values);
        },
        validationSchema,
        validateOnMount: true,
        enableReinitialize: true
    })
    const onRowUpdated = (e) => {
        console.log(e)
    }
    const onSaved = (e) => {
        console.log(e)
    }




    return <>


        <form onSubmit={formik.handleSubmit}>
            <div
                className="grid gap-4   xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-6 grid-flow-row">
                <DateBox
                    formik={formik} placeholder='تاریخ ثبت' name={'InvoiceDate'}
                />

                <Input formik={formik} name={'PersonID'} type={'tel'} placeholder={'نوع شخص خریدار'}/>

                <Switch defaultValue={true} width={70} name={'InvoiceAddDate'} onValueChange={(e) => {
                    formik.setFieldValue('InvoiceAddDate', e);
                }} switchedOffText={'پرواز داخلی'} switchedOnText={'پرواز خارجی'}/>
                <Input formik={formik} name={'InternalInvoiceNo'} type={'tel'}
                       placeholder={'شماره کوتاژ اظهار نامه گمرکی'}/>


                <DateBox
                    formik={formik} placeholder='تاریخ  کوتاژ اظهار نامه گمرکی' name={'RefInvoiceID'}
                />
                <Input formik={formik} name={'CashPaymentAmount'} type={'tel'}
                       placeholder={'شماره اشتراک/ شناسه قبض بهره بردار'}/>


            </div>


            <DataGrid

                onRowUpdated={onRowUpdated}
                onSaved={onSaved}
                onSaving={onSaving}
                cusedRowChanged={(e) => console.log(e)}
                ref={dataGridRef}
                keyExpr={'ID'}
                id={`CreateInvoice${pattern}`}
                dataSource={data}
                columnAutoWidth={true}
                rtlEnabled={true}
                allowSorting={false}
                onFocusedCellChanged={onFocusedCellChanged}
                onFocusedCellChanging={onFocusedCellChanging}
                showBorders={true}
                allowColumnResizing={true}
                columnResizingMode={'nextColumn'}
                repaintChangesOnly
                allowAdding={true}
                allowUpdating={true}
                useIcons={true}
                showColumnLines={true}
                showRowLines={true}
            >
                <Scrolling columnRenderingMode="virtual"/>
                <RowDragging
                    allowReordering={true}
                    onReorder={onReorder}
                    showDragIcons
                />

                <Editing
                    allowSorting={false}
                    allowDeleting
                    useIcons
                    mode="batch" allowUpdating={true} allowAdding={true}
                    newRowPosition={'last'}


                    startEditAction="dblClick"
                 >



                    <Texts
                        deleteRow="Remove"
                        addRow="eee"
                    />
                </Editing>



                <KeyboardNavigation editOnKeyPress={true} enterKeyAction={'moveFocus'} enterKeyDirection={'column'}/>


                <Paging enabled={false}/>
                <StateStoring enabled={true} type="localStorage" storageKey="storage"/>
                <ColumnFixing enabled={true}/>
                <Grouping autoExpandAll={false}/>


                <Column dataField='ProductID' caption='شناسه کالا/خدمات'>
                    <RequiredRule message={'hhhhhhhh'}  />
                </Column>
                <Column dataField='ProductDescription' caption='شرح کالا/خدمات'/>
                <Column dataField='Value' caption='تعداد/ مقدار'/>
                <Column dataField='UnitAmount' caption='واحد اندازه گیری'/>
                <Column dataField='DiscountAmount' caption='مبلغ واحد'/>
                {/*<Column dataField='Amount' caption='نرخ مالیات بر ارزش افزوده'/>*/}
                {/*<Column dataField='status' caption='مبلغ مالیات بر ارزش افزوده'/>*/}
                {/*<Column dataField='currencyConversionTable' caption='شناسه یکتای ثبت قرارداد حق العمل کاری'/>*/}
                {/*<Column dataField='date' caption='مبلغ کل کالا/ خدمات'/>*/}
                {/*<Column dataField='w' visible={[1, 2, 3, 4, 6].includes(pattern)} caption='میزان ارز'/>*/}
                {/*<Column dataField='e' visible={[1, 2, 3, 4, 6, 7].includes(pattern)} caption='نوع ارز'/>*/}
                {/*<Column dataField='r' visible={[1, 2, 3, 4, 6, 7].includes(pattern)} caption='نرخ برابری ارز با ریال'/>*/}
                {/*<Column dataField='t' visible={[1, 2, 3, 4, 5].includes(pattern)} caption='مبلغ قبل از تخفیف'/>*/}
                {/*<Column dataField='t' visible={[1, 2, 3, 4, 5].includes(pattern)} caption='مبلغ تخفیف'/>*/}
                {/*<Column dataField='trr' visible={[1, 2, 3, 4, 5].includes(pattern)} caption='مبلغ بعد از تخفیف'/>*/}
                {/*<Column dataField='ewe' visible={[1, 2, 3, 4, 5, 7].includes(pattern)}*/}
                {/*        caption='موضوع سایر  مالیات و عوارض'/>*/}
                {/*<Column dataField='sdv' visible={[1, 2, 3, 4, 5, 7].includes(pattern)}*/}
                {/*        caption='نرخ سایر  مالیات و عوارض'/>*/}
                {/*<Column dataField='dd' visible={[1, 2, 3, 4, 5, 7].includes(pattern)}*/}
                {/*        caption='مبلغ سایر  مالیات و عوارض'/>*/}
                {/*<Column dataField='cv' visible={[1, 2, 3, 4, 5, 7].includes(pattern)}*/}
                {/*        caption='موضوع سایر  وجوه قانونی'/>*/}
                {/*<Column dataField='ass' visible={[1, 2, 3, 4, 5, 7].includes(pattern)} caption='نرخ سایر  وجوه قانونی'/>*/}
                {/*<Column dataField='vgh' visible={[1, 2, 3, 4, 5, 7].includes(pattern)} caption='مبلغ سایر وجوه قانونی'/>*/}
                {/*<Column dataField='bgfd' visible={[1, 2, 3, 4].includes(pattern)} caption='سهم نقدی از پرداخت'/>*/}
                {/*<Column dataField='rew' visible={[1, 2, 3, 4].includes(pattern)}*/}
                {/*        caption='سهم مالیات بر ارزش افزوده از پرداخت'/>*/}
                {/*<Column dataField='gtre' visible={[3].includes(pattern)} caption='اجرت ساخت'/>*/}
                {/*<Column dataField='sdfgtg' visible={[3].includes(pattern)} caption='سود فروشنده'/>*/}
                {/*<Column dataField='cxdxs' visible={[3].includes(pattern)} caption='حق العمل'/>*/}
                {/*<Column dataField='zsdxsecr' visible={[3].includes(pattern)} caption='جمع کل اجرت، حق العمل و سود'/>*/}
                {/*<Column dataField='sdcsd' visible={[7].includes(pattern)} caption='وزن خالص'/>*/}
                {/*<Column dataField='asdae' visible={[7].includes(pattern)} caption='ارزش ریالی کالا'/>*/}
                {/*<Column dataField='weewwcec' visible={[7].includes(pattern)} caption='ارزش ارزی کالا'/>*/}
                {/**/}
                {/**/}
                {/*<Column dataField='currencyConversionTable' caption='قیمت 3' format="currency"*/}
                {/*        calculateCellValue={fullNameColumn_calculateCellValue}*/}
                {/*/>*/}
                <Summary recalculateWhileEditing={true}>
                    <TotalItem
                        column="currencyConversionTable"
                        summaryType="sum"
                        valueFormat="currency"/>

                </Summary>
                <Column type='buttons' caption='نوع سند' fixed={true} fixedPosition={'left'}>
                    <DevButton name='delete' onClick={(e) => handleDelete(e)}/>
                </Column>

                {/*<Toolbar>*/}
                {/*    <Item name="addRowButton" showText="always"/>*/}
                {/*    <Item name="addRowButton" showText="always"/>*/}
                {/*    <Item name="addRowButton" showText="always"/>*/}
                {/*</Toolbar>*/}
            </DataGrid>

            <div
                className="grid gap-4   xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-6 grid-flow-row">


                <Input formik={formik} name={'LoanPaymentAmount'} placeholder={'شماره اشتراک/ شناسه قبض بهره بردار'}/>
                <Input formik={formik} name={'ProductDescription'} placeholder={'شماره اشتراک/ شناسه قبض بهره بردار'}/>
                <Input formik={formik} name={'UnitAmount'} placeholder={'شماره اشتراک/ شناسه قبض بهره بردار'}/>
            </div>
            <div className="flex items-center justify-end ml-4">
                <Button loading={loading} type="submit">ثبت</Button>
            </div>
        </form>
    </>;
}

export default CreateInvoice;

