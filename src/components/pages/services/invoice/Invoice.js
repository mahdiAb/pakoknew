import React, {useEffect} from 'react';
import LayoutServices from "../../../layout/LayoutServices";
import {connect} from "react-redux";
import {handleInvoiceActiveKey, handleInvoiceNewTabIndex, handleInvoiceTabList} from "../../../redux/reducer/Reducer";
import {GiMoneyStack} from "@react-icons/all-files/gi/GiMoneyStack";
import CreateInvoice from "./tabs/CreateInvoice";

let items = [{

    title: 'ثبت تکی صورت حساب',
    icon: <GiMoneyStack size={20}/>,
    children: [
        {content: <CreateInvoice pattern={1}/>, title: 'فروش'},
        {content: <CreateInvoice pattern={2}/>, title: 'فروش ارزی'},
        {content: <CreateInvoice pattern={3}/>, title: 'طلا جواهر و پلاتین'},
        {content: <CreateInvoice pattern={4}/>, title: 'قرداد پیمانکاری'},
        {content: <CreateInvoice pattern={5}/>, title: 'قبوض خدماتی'},
        {content: <CreateInvoice pattern={6}/>, title: 'بلیط هواپیما'},
        {content: <CreateInvoice pattern={7}/>, title: 'صادرات'}
        ]
},{content: <CreateInvoice pattern={1}/>, title: 'ثبت گروهی صورت حساب',},]

const Invoice = ({
                 handleInvoiceTabList, handleInvoiceActiveKey, handleInvoiceNewTabIndex,
                 newTab, tabListState, activeKey, newTabIndex,
             }) => {

    useEffect(() => {

        if (newTab) {
            add({content: newTab.content, title: newTab.tabTitle})
        }
    }, [newTab])


    const add = (data) => {
        handleInvoiceNewTabIndex()


        const newPanes = [...tabListState];
        newPanes.push({
            title: data.title, content: data.content, id: newTabIndex,
        });
        handleInvoiceTabList(newPanes)

        handleInvoiceActiveKey(newTabIndex);

    };

    const remove = (item) => {
        const newTabListState = [...tabListState];
        const index = newTabListState.indexOf(item);

        newTabListState.splice(index, 1);


        if (index >= newTabListState.length && index > 0) {
            handleInvoiceActiveKey(newTabListState[index - 1]);
        }

        handleInvoiceTabList(newTabListState)


    };


    const handleInvoiceActive = (x) => {
        handleInvoiceActiveKey(x)
    }
    return (


        <LayoutServices tabs={tabListState} items={items}
                        activeKey={activeKey}
                        handleNewTabIndex={handleInvoiceNewTabIndex}
                        handleCloseTab={remove}
                        handleAddTab={add}
                        handleActiveKey={handleInvoiceActive}
                        handleTabList={handleInvoiceTabList}
        />

    );
};


function mapStateToProps(state) {
    return {
        tabListState: state.invoiceTabList,
        newTabIndex: state.invoiceNewTabIndex,
        activeKey: state.invoiceActiveKey,
        newTab: state.invoiceNewTab,
    }
}


export default connect(mapStateToProps, {
    handleInvoiceTabList,
    handleInvoiceActiveKey,
    handleInvoiceNewTabIndex
})(Invoice);

