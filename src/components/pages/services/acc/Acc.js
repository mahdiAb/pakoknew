import React, {useEffect} from 'react';
import LayoutServices from "../../../layout/LayoutServices";
import {connect} from "react-redux";
import {handleAccActiveKey, handleAccNewTabIndex, handleAccTabList} from "../../../redux/reducer/Reducer";
import {GiMoneyStack} from "@react-icons/all-files/gi/GiMoneyStack";

let items = [{

    title: '111cc1',
    icon: <GiMoneyStack size={20}/>,
    children: [
        {content: <p>jjj</p>, title: '22222'},
        {
            icon: <GiMoneyStack size={20}/>,
            title: '33333',
            children: [{content: <p>jjj</p>, title: '44444'}, {content: <p>jjj</p>, title: '5555'},]
        },]
}]

const Acc = ({
                 handleAccTabList, handleAccActiveKey, handleAccNewTabIndex,
                 newTab, tabListState, activeKey, newTabIndex,
             }) => {

    useEffect(() => {

        if (newTab) {
            add({content: newTab.content, title: newTab.tabTitle})
        }
    }, [newTab])


    const add = (data) => {
        handleAccNewTabIndex()


        const newPanes = [...tabListState];
        newPanes.push({
            title: data.title, content: data.content, id: newTabIndex,
        });
        handleAccTabList(newPanes)

        handleAccActiveKey(newTabIndex);

    };

    const remove = (item) => {
        const newTabListState = [...tabListState];
        const index = newTabListState.indexOf(item);

        newTabListState.splice(index, 1);


        if (index >= newTabListState.length && index > 0) {
            handleAccActiveKey(newTabListState[index - 1]);
        }

        handleAccTabList(newTabListState)


    };


    const handleAccActive = (x) => {
        handleAccActiveKey(x)
    }
    return (


        <LayoutServices tabs={tabListState} items={items}
                        activeKey={activeKey}
                        handleNewTabIndex={handleAccNewTabIndex}
                        handleCloseTab={remove}
                        handleAddTab={add}
                        handleActiveKey={handleAccActive}
                        handleTabList={handleAccTabList}
        />

    );
};


function mapStateToProps(state) {
    return {
        tabListState: state.accTabList,
        newTabIndex: state.accNewTabIndex,
        activeKey: state.accActiveKey,
        newTab: state.accNewTab,
    }
}


export default connect(mapStateToProps, {
    handleAccTabList,
    handleAccActiveKey,
    handleAccNewTabIndex
})(Acc);

