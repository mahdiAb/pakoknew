import React, {useState} from 'react';
import Input from "../../../../microComponents/common/uikits/Input";
import Button from "../../../../microComponents/common/uikits/Button";
import {useFormik} from "formik";
import * as Yup from "yup";
import {validation} from "../../../../helper/validation/Validation";
import show from "../../../../helper/Notify/Notify";
import {useNavigate, useParams} from "react-router-dom";
import {CreatePerson} from "../../../../helper/apiList/apiListBusiness";
import GetAddress from "../../../../microComponents/common/GetAddress/GetAddress";
import {Switch} from 'devextreme-react/switch';
import RadioGroup from 'devextreme-react/radio-group';
import BusinessRequest from "../../../../helper/request/BusinessRequest";

let initialValues = {
    FirstName: '',
    LastName: '',
    PhoneNumber: '',
    PostalCode: '',
    NationalID: '',
    RegistrationNumber: '',
    CityID: '',
    StateID: '',
    Fax: '',
    PersonTypeID: true,
    EconomicalNumber: "",
    RoleID: "1",
    PersonCategoryID: "",
    AddressDTOs: [{
        CityID: "", StateID: "", Extra: ""
    }],
}
const CreatePersons = () => {
    const navigate = useNavigate();
    let {id} = useParams();
    let [loading, setLoading] = useState(false)
    const handleCreatePerson = (values) => {
        let x ={

            "FirstName": "هما",
            "LastName": "بهمنی",
            "PhoneNumber": "4488966415",
            "PersonTypeID": "1",
            "EconomicalNumber": "5555",
            "NationalID": "8865",
            "PostalCode": "55874",
            "RegistrationNumber": "42545",
            "CityID": "01d1c8f5-c146-415c-9e54-3f5e676acbf2",
            "StateID": "33fef24b-320e-4078-95da-3a67f747d5db",
            "Fax": "7554774",
            "RoleID": "1",
            "PersonCategoryID" : "1",
            "AddressDTOs" :
                [
                    {
                        "ID": "ff1fd101-fa7b-46c1-ac93-6d582ba728b3",
                        "CountryID": "4fd9662c-1d2e-4861-abe6-2adca71e2c35",
                        "CityID": "01d1c8f5-c146-415c-9e54-3f5e676acbf2",
                        "Region": "ولیعصر",
                        "Street": "پنجم",
                        "Alley": "اول",
                        "Plaque": "33",
                        "Extra": "ssss",
                        "PersonID" : "7a83464f-6267-4c05-9a42-d0d5bf92d6a4"

                    }
                ]
        }
        setLoading(true)

        values.AddressDTOs[0].StateID = values.StateID
        values.AddressDTOs[0].CityID = values.CityID
        values.AddressDTOs[0].Extra = values.Extra
        BusinessRequest.init(CreatePerson, function (data) {
            setLoading(false)
            if (data.data.messageID === 0) {
                show('success', 'کاربر با مفقیت ثبت شد')
            } else {
                show('error', data.data.message)

            }
        }, function (error) {
            setLoading(false)

        }).setBID(id).setRouter(navigate).setMainData(x).callRequest()
    }


    const validationSchema = Yup.object().shape({
        PhoneNumber: validation.mobile('شماره موبایل'),
        FirstName: validation.string('نام'),
        LastName: validation.string('نام خانوادگی'),
        NationalID: validation.number10('کد ملی'),
        PostalCode: validation.number10('کد پستی'),
        RegistrationNumber: validation.number10('شماره ثبت'),
        PersonTypeID: validation.string('نوع کاربر'),
        EconomicalNumber: validation.number12('شماره اقتصادی'),
        // PersonCategoryID: validation.string('شماره اقتصادی'),

        Extra: validation.string('آدرس کامل'),


    })


    const formik = useFormik({
        initialValues: initialValues,
        onSubmit: values => {
            handleCreatePerson(values);
        },

        validateOnMount: true,
        enableReinitialize: true
    })


    return (<div>
        <form onSubmit={formik.handleSubmit}>
            <div
                className="grid gap-4   xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-6 grid-flow-row">
                <Switch defaultValue={true} width={70} name={'PersonTypeID'} onValueChange={(e) => {
                    formik.setFieldValue('PersonTypeID', e);
                }}/>
                <Input formik={formik} placeholder='نام' name={'FirstName'}/>
                <Input formik={formik} placeholder='نام خانوادگی' name={'LastName'}/>
                <Input formik={formik} name={'PhoneNumber'} type={'tel'} placeholder={'شماره موبایل'}/>
                <Input formik={formik} name={'PostalCode'} type={'tel'} placeholder={'کد پستی'}/>
                <Input formik={formik} name={'NationalID'} type={'tel'} placeholder={'کد ملی'}/>
                <Input formik={formik} name={'RegistrationNumber'} type={'tel'} placeholder={'شماره ثبت'}/>
                <Input formik={formik} name={'Fax'} type={'tel'} placeholder={'شماره فکس'}/>
                <GetAddress formik={formik}/>
                <Input formik={formik} name={'EconomicalNumber'} type={'tel'} placeholder={'شماره اقتصادی'}/>
                <RadioGroup
                    // value={this.state.selectionPriority}
                    name={'PersonCategoryID'}
                    valueExpr="id"
                    displayExpr="text"
                    onValueChange={(e) => {
                        formik.setFieldValue('PersonCategoryID', e);
                    }}


                    items={[
                        { id: 1,value: 1, text: 'غیر دولتی'},
                         {id: 2,value: 2, text: 'دولتی'},
                        {id: 3,value: 3, text: ' بین المللی'},
                    ]} defaultValue={1}/>


            </div>
            <div className="flex items-center justify-end ml-4">
                <Button loading={loading} type="submit">ثبت</Button>
            </div>
        </form>
    </div>);
};

export default CreatePersons;