import React, {useEffect} from 'react';
import LayoutServices from "../../../layout/LayoutServices";
import {connect} from "react-redux";
import {handleSettingActiveKey, handleSettingNewTabIndex, handleSettingTabList} from "../../../redux/reducer/Reducer";
import {GiMoneyStack} from "@react-icons/all-files/gi/GiMoneyStack";
import CreatePerson from "./Users/CreatePerson";
import PersonList from "./Users/PersonList";


let items = [
    {
        icon: <GiMoneyStack size={20}/>,
        title: 'اشخاص',
        children: [{icon: <GiMoneyStack size={20}/>, content: <CreatePerson/>, title: 'ایجاد شخص'},
            {icon: <GiMoneyStack size={20}/>, content: <PersonList/>, title: 'لیست اشخاص'},]
    },

]

const Setting = ({handleSettingTabList, handleSettingActiveKey, handleSettingNewTabIndex,
                     newTab, tabListState, activeKey, newTabIndex,
                 }) => {


    useEffect(() => {

        if (newTab) {
            add({content: newTab.content, title: newTab.tabTitle})
        }
    }, [newTab])


    const add = (data) => {


        handleSettingNewTabIndex()


        const newPanes = [...tabListState];
        newPanes.push({
            title: data.title, content: data.content, id: newTabIndex,
        });
        handleSettingTabList(newPanes)

        handleSettingActiveKey(newTabIndex);

    };

    const remove = (item) => {
        const newTabListState = [...tabListState];
        const index = newTabListState.indexOf(item);

        newTabListState.splice(index, 1);


        if (index >= newTabListState.length && index > 0) {
            handleSettingActiveKey(newTabListState[index - 1]);
        }

        handleSettingTabList(newTabListState)


    };


    const handleSettingActive = (x) => {
        handleSettingActiveKey(x)
    }
    return (


        <LayoutServices tabs={tabListState} items={items}
                        activeKey={activeKey}
                        handleNewTabIndex={handleSettingNewTabIndex}
                        handleCloseTab={remove}
                        handleAddTab={add}
                        handleActiveKey={handleSettingActive}
                        handleTabList={handleSettingTabList}


        />

    );
};


function mapStateToProps(state) {
    return {
        tabListState: state.settingTabList,
        newTabIndex: state.settingNewTabIndex,
        activeKey: state.settingActiveKey,
        newTab: state.settingNewTab,
    }
}


export default connect(mapStateToProps, {
    handleSettingTabList,
    handleSettingActiveKey,
    handleSettingNewTabIndex
})(Setting);

