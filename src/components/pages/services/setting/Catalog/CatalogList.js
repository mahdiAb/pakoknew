import React, {useEffect, useState} from 'react';
import DataGrid, {
    Column,
    ColumnChooser,
    FilterRow,
    Grouping,
    GroupPanel,
    HeaderFilter,
    Item,
    KeyboardNavigation,
    LoadPanel,
    Pager,
    Paging,
    SearchPanel,
    Selection,
    StateStoring,
    Toolbar,
} from 'devextreme-react/data-grid';
import {TreeView} from "devextreme-react";

import {useNavigate, useParams} from "react-router-dom";
import show from "../../../../helper/Notify/Notify";
import {GetAllPerson, RegisterUser} from "../../../../helper/apiList/apiListBusiness";
import Request from "../../../../helper/request/Request";
import Button from "../../../../microComponents/common/uikits/Button";
import BusinessRequest from "../../../../helper/request/BusinessRequest";


const CatalogList = () => {

    let {id} = useParams();


    const [loading, setLoading] = useState(false);

    const [data, setData] = useState([]);


    const navigate = useNavigate();


    const printTable = () => {
        console.log('print')
    }


    const createUser = (values) => {

        setLoading(true)
        Request.init(RegisterUser, function (data) {
            setLoading(false)
            if (data.data.messageID == 0) {
                show({type: 'success', description: data.data.message,})
            } else {
                show({type: 'error', description: data.data.message})


            }
        }, function (error) {
            show({type: 'error', description: 'لطفا دوباره امتحانن کنید!'})
            setLoading(false)

        }).loginPages().setRouter(navigate).setMainData(values).callRequest()
    };
    const searchUser = (values) => {

        setLoading(true)
        BusinessRequest.init(RegisterUser, function (data) {
            setLoading(false)
            if (data.data.messageID == 0) {

                show({type: 'error', description: data.data.message})


            } else {

                show({type: 'error', description: data.data.message})


            }
        }, function (error) {
            show({type: 'error', description: 'لطفا دوباره امتحانن کنید!'})
            setLoading(false)

        }).setRouter(navigate).setMainData(values).callRequest()
    };

     useEffect(() => {

        (async () => {


            await BusinessRequest.init(GetAllPerson, function (data) {
                setData(data.data)

                setLoading(false)
            }, function (error) {

                setLoading(false)
            }).setBID(id).setRouter(navigate).callRequest()

        })();
    }, [navigate]);

    return (
        <div>

            <div>
                <DataGrid

                    id='gridContainer'
                    dataSource={data}
                    columnAutoWidth={true}
                    keyExpr="ID"
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnResizingMode={'widget'}
                    showColumnLines={true}
                    showRowLines={false}
                    showBorders={true}
                    rowAlternationEnabled={true}
                    onRowDblClick={(e) => console.log(e)}>

                    <ColumnChooser enabled={true}/>
                    <LoadPanel enabled={true}/>
                    <Selection
                        mode="multiple"
                        selectAllMode={'allPages'}
                        showCheckBoxesMode={'onClick'}
                    />
                    <HeaderFilter visible={true}/>
                    <Grouping autoExpandAll={false}/>
                    <SearchPanel visible={true}/>
                    <GroupPanel visible={true}/>
                    <KeyboardNavigation editOnKeyPress={false} enterKeyAction={'moveFocus'}
                                        enterKeyDirection={'column'}/>
                    <Pager
                        visible={true}
                        allowedPageSizes={10}
                        displayMode={'full'}
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true}/>
                    <Paging defaultPageSize={10}/>
                    <StateStoring enabled={true} type="localStorage" storageKey="storage"/>
                    <FilterRow onFilterValueChange={(e) => console.log(e)}
                               visible={true} applyFilter="auto"/>

                    <Column width={100} dataField='Picture' cellRender={(data) => <img src={data.value}/>}
                            caption='تصویر کاربر'/>
                    <Column dataField='name' caption='نام' dataType='number'/>
                    <Column dataField="family" caption="نام خانوادکی" width={150} allowSorting={true}/>
                    <Column dataField='createDate' caption='تاریخ ثبت نام'>
                        <HeaderFilter>
                            <TreeView id="simple-treeview"
                                // items={AmountHeaderFilter}
                                      width={300}
                            />
                        </HeaderFilter>
                    </Column>
                    <Column dataField='status' caption='وضعیت'/>
                    <Column dataField='number' caption='شماره تماس'/>

                    <Column dataField='lastActive' caption='نوع سند'>
                        <div>jhjh</div>
                    </Column>
                    <Column dataField='lastActive' caption='آخرین فعالیت'/>
                    <Column width={100} dataField='action' cellRender={() => <p>{' ShowFull/>'}</p>}
                            caption='تصویر کاربر'/>
                    <Toolbar>

                        <Item location="before">
                            {/*<AddUser/>*/}
                        </Item>

                        <Item location="after">

                        </Item>
                        <Item location="after">
                            <Button icon="print"
                                    text="Print"
                                    onClick={printTable}
                            />
                        </Item>

                    </Toolbar>

                </DataGrid>
            </div>
        </div>
    );
};

export default CatalogList;