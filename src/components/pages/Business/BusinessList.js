import React, {useEffect, useState} from 'react';

import Cookies from 'js-cookie';
import {HiMenuAlt1} from "@react-icons/all-files/hi/HiMenuAlt1";

import {NavLink, useNavigate} from "react-router-dom";
import {GetBusinessList, GetBusinessToken} from "../../helper/apiList/apiListBusiness";
import Request from "../../helper/request/Request";
import Layout from "../../microComponents/common/Layout/Layout";
import KeyValue from "../../microComponents/common/keyValue/KeyValue";
import Loading from "../../microComponents/common/loading/Loading";
import BusinessListLoading from "../../skeletonLoading/BusinessListLoading";
import ActiveServices from "./ActiveServices";
import {Button, Dropdown, Menu, Progress} from "antd";


const BusinessList = () => {
    const [getLoading, setGetLoading] = useState(true);
    const [checkLoading, setCheckLoading] = useState(true);
    const [pushToBusinessLoading, setPushToBusinessLoading] = useState(false);
    const [selectedBusinessID, setSelectedBusinessID] = useState(false);
    const [businessList, setBusinessList] = useState([]);


    const navigate = useNavigate();


    useEffect(() => {
        if (Cookies.get('Token')) {
            setCheckLoading(false)
        } else {
            navigate('/login');
        }
        (async () => {


            await Request.init(GetBusinessList, function (data) {
                setBusinessList(data.data)
                setGetLoading(false)
            }, function (error) {
                console.log(error)
                setGetLoading(false)
            }).setRouter(navigate).callRequest()

        })();
    }, [navigate]);


    const pushToBusiness = async (item) => {
        let data = {
            BusinessID: item.businessID
        }
        setSelectedBusinessID(item.businessID)
        setPushToBusinessLoading(true)

        await Request.init(GetBusinessToken, async function (res) {
            await Cookies.set(`BusinessToken_${item.businessID}`, res.data.jwtToken, {expires: 7})
            await Cookies.set(`BusinessRefreshToken_${item.businessID}`, res.data.refreshToken, {expires: 7})

            navigate('/business/' + item.businessID);
            setPushToBusinessLoading(false)
        }, function (error) {

            setPushToBusinessLoading(false)
        }).setRouter(navigate).setMainData(data).callRequest()


    }


    let dropDownOptions = {

        width: 150

    }
    const menu = (item) => {
        let items = [
            // {key: '1', label: (<NavLink to="/">ویرایش</NavLink>)},
            // {type: 'divider'},

            {key: '1', label: (<NavLink to="/userManagement">مدیریت کاربران</NavLink>)},
            {type: 'divider'},


            {key: '2', label: (<div className='text-danger'>حذف</div>)},

        ]
        let MultiBranch = [
            {key: '3', label: (<NavLink to={`/branchManagement`}>مدیریت شعب</NavLink>)},
            {type: 'divider'},
        ]
        let addItems = [
            {key: '4', label: (<NavLink to={`/ExtensionBusiness/${item.businessID}`}>تمدید اشتراک</NavLink>)},
            {type: 'divider'},
        ]
        let addItems2 = [
            {key: '5    ', label: (<NavLink to={`/upgradeBusiness/${item.businessID}`}>ارتقا اشتراک</NavLink>)},
            {type: 'divider'}
        ]
        if (item.serviceList.length > 0 && !item.unPaidOrder) {
            items = [...addItems, ...items]
            if (item.maxNumberOfDay > 0) {
                items = [...addItems2, ...items]
            }
        }
        if (item.isMultiBranch) {
            items = [...MultiBranch, ...items]
        }
        return <Menu
            items={items}
        />
    }
    return (<>
            {checkLoading ? <Loading/> : <Layout>

                <div className="p-4 max-w-7xl m-auto ">
                    <div className="flex items-center justify-between mb-3">
                        <h3 className="font-bold ">لیست کسب و کارها</h3>
                        <NavLink to="/createBusiness" className='bg-blue text-white rounded-xl p-2 '> ایجاد کسب و کار </NavLink>
                    </div>
                    {!getLoading ? <>
                        {businessList?.map((item, index) => (

                            <div key={index} className="border-2 border-black-200 p-3 mb-5 rounded-2xl">
                                <div className="">

                                    <div className="flex justify-between items-start">
                                        <div className="flex items-center">
                                            <img className=' ' width={60} src="/logo.png" alt="logo"/>
                                            <h5 className='mx-2 font-bold'>{item.businessName}</h5>

                                        </div>

                                        <div className="flex items-center">


                                            {item.isOwner && <><ActiveServices serviceList={item.serviceList}/>


                                                <Dropdown placement="bottomLeft" arrow
                                                          overlay={menu(item)}
                                                          overlayStyle={{width: '180px'}}
                                                          trigger={['click']}>

                                                    <Button
                                                        className='bg-black-200 text-black font-medium p-2 w-7 h-7  black'><HiMenuAlt1/>
                                                    </Button>
                                                </Dropdown></>}


                                        </div>
                                    </div>
                                    <div className="flex items-center w-100 justify-between my-4">
                                        <div className="">


                                            <KeyValue value={'PAKOK123'} keyText={'صاحب کسب و کار'}/>
                                            <KeyValue value={item.businessTypeName}
                                                      keyText={'صنف کسب و کار'}/>
                                            <KeyValue value={'PAKOK123'} keyText={'کد کسب و کار'}/>
                                            <KeyValue value={item.maxNumberOfDay}
                                                      keyText={'روز باقی مانده'}
                                                      className={item.maxNumberOfDay < 10 && 'dangerBox '}/>

                                        </div>
                                        <div className=''>


                                            <Progress
                                                strokeColor={{
                                                    '0%': '#108ee9',
                                                    '100%': '#87d068',
                                                }}

                                                type="circle" strokeLinecap="butt" width={100} strokeWidth={7}
                                                percent={77}
                                                format={percent =>
                                                    <div>

                                                                  <span className='text-base font-bold'>
                                                                      {item.usedNumberOfUser}/{item.maxNumberOfUser}
                                                                  </span>
                                                        <p
                                                            className='mb-0 text-sm'>تعداد
                                                            کاربران</p>


                                                    </div>}
                                            > </Progress>


                                        </div>
                                    </div>
                                    <div className="flex justify-end items-center mt-2">


                                        {item.unPaidOrder ?

                                            <NavLink to={`/order/${item.businessID}`}
                                                     className='flex-1 text-blue ml-2'>صورت
                                                حساب</NavLink>

                                            : null


                                        }
                                        {!item.serviceList.length && !item.unPaidOrder &&

                                            <NavLink to={`/ExtensionBusiness/${item.businessID}`}
                                                     className='defBtn  flex-1 bigBtnGreen borderBtn  '>خرید
                                                اشتراک</NavLink>


                                        }
                                        {item.maxNumberOfDay ?


                                            <Button
                                                loading={item.BusinessID === selectedBusinessID && pushToBusinessLoading}
                                                onClick={() => pushToBusiness(item)}
                                                className='  bg-blue text-white  max-w-xs flex-1 h-10'>ورود
                                                به
                                                سیستم</Button>

                                            : null


                                        }


                                    </div>

                                </div>
                            </div>

                        ))}
                    </> : <BusinessListLoading/>

                    }


                </div>
            </Layout>}</>

    );
}


export default BusinessList;

