import React, {useState} from 'react';
import 'devextreme/dist/css/dx.light.css';

import {Popup} from 'devextreme-react/popup';
import {BiFoodMenu} from "@react-icons/all-files/bi/BiFoodMenu";
import {Button} from "antd";

const renderContent = (serviceList) => {
    return (<div
        className="grid gap- xs:grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3   grid-flow-row">

        {serviceList.map((serviceItem, serviceIndex) => (<>
            <div key={serviceIndex} className="border-black-200 border-2 rounded-2xl mx-3 p-3">
                <div className="h-auto">
                    <div className=" ">
                        <div className='flex items-center justify-between'>

                            <div className="ml-4 ">
                                <div className="mb-4">
                                    {serviceItem.serviceName}</div>
                            </div>

                            <img width={100} src={'/logo.png'}/>
                        </div>


                    </div>

                    {serviceItem.moduleList.length ? <div className="inner w-100">
                        <ul className='p-0'> {serviceItem.moduleList.map((item, index) => (
                            <li key={index}><p>{item.moduleName}</p></li>))}
                        </ul>
                    </div> : null}

                </div>
            </div>
        </>))}

    </div>);
}
const ActiveServices = ({serviceList}) => {
    console.log(serviceList)
    const [isPopupVisible, setPopupVisibility] = useState(false);

    const togglePopup = () => {
        setPopupVisibility(!isPopupVisible);
    };


    return (<div className={'flex'}>
        <Popup
            contentRender={() => renderContent(serviceList)}
            visible={isPopupVisible}
            hideOnOutsideClick={true}
            width={'90%'}
            maxWidth={800}
            height={500}
            onHiding={togglePopup}
            title={'لیست سرویسها و زیر سرویس ها'}
        />
        <Button onClick={togglePopup}
                className='bg-blue-300 text-blue   p-2 w-8 h-8 ml-2  flex items-center justify-center'><BiFoodMenu
            size={40}/>
        </Button>


    </div>);
};

export default ActiveServices;