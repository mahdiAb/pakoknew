import React, {useState} from 'react';
import Input from "../../../microComponents/common/uikits/Input";
import {useFormik} from "formik";
import * as Yup from "yup";
import {validation} from "../../../helper/validation/Validation";
import {useNavigate} from "react-router-dom";
import Button from "../../../microComponents/common/uikits/Button";
import Request from "../../../helper/request/Request";
import show from "../../../helper/Notify/Notify";
import OtpBox from "../OtpBox";
import {RegisterUser} from "../../../helper/apiList/apiListBusiness";
import Checkbox from "../../../microComponents/common/uikits/Checkbox";


let initialValues = {
    rule: false,
    fname: '',
    lname: '',
    MobileNumber: '',

}


const Register = () => {
    let [step, setStep] = useState(1)
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);


    const validationSchema = Yup.object().shape({
        MobileNumber: validation.mobile('شماره موبایل'),
        fname: validation.string('نام'),
        lname: validation.string('نام خانوادگی'),
        rule: validation.checkbox('برای ثبت نام لازم است موافقت خود با قوانین را اعلام کنید!'),

    })


    const handleGenerateSMSOTP = (values) => {
        setLoading(true)


        Request.init(RegisterUser, function (data) {
            setLoading(false)
            if (data.data.messageID === 0) {
                setStep(2)
            } else {
                show('error', data.data.message)

            }
        }, function (error) {
            setLoading(false)

        }).loginPages().setRouter(navigate).setMainData(values).callRequest()


    };


    const formik = useFormik({
        initialValues: initialValues,
        onSubmit: handleGenerateSMSOTP,
        validationSchema,
        validateOnMount: true,
        enableReinitialize: true
    })
    return (
        <div className={'p-3'}>
            {
                step === 1 ?
                    <form onSubmit={formik.handleSubmit}>

                        <Input formik={formik} placeholder='نام' name={'fname'}/>
                        <Input formik={formik} placeholder='نام خانوادگی' name={'lname'}/>
                        <Input formik={formik} name={'MobileNumber'} type={'tel'} placeholder={'شماره موبایل'}/>


                        <Checkbox formik={formik} name={'rule'} type={'tel'}
                                  placeholder={'قوانین و مقررات سیم خان را مطالعه نموده و با کلیه موارد آن موافقم.'}/>


                        <div className="flex items-center justify-end">


                            <Button loading={loading} type="submit" disabled={!formik.isValid}>ثبت</Button>

                        </div>
                    </form>
                    :

                    <OtpBox setStep={() => setStep(1)} addValues={formik.values}
                            sendPhone={formik.handleSubmit}
                            loadingSendPhone={loading}/>
            } </div>)
}


export default Register;