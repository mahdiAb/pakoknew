import React, {useEffect, useState} from 'react';
import Input from "../../microComponents/common/uikits/Input";
import Button from "../../microComponents/common/uikits/Button";
import Request from "../../helper/request/Request";
import {ChangeForgottenPassword, Login} from "../../helper/apiList/apiListBusiness";
import show from "../../helper/Notify/Notify";
import {useFormik} from "formik";
import {useNavigate} from "react-router-dom";
import * as Yup from "yup";
import {validation} from "../../helper/validation/Validation";

import Cookies from 'js-cookie';

let initialValues = {
    otp: ''
}
const OtpBox = ({
                    addValues,
                    setStep, sendPhone,
                    url = Login,
                    loadingSendPhone = false,
                }) => {
    const [timeLeft, setTimeLeft] = useState(120);
     const [disableTimer, setDisableTimer] = useState(true);


    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        if (!timeLeft) {
            setDisableTimer(false)
            return
        }
        const intervalId = setInterval(() => {
            setTimeLeft(timeLeft - 1);
        }, 1000);

        return () => {
            clearInterval(intervalId)
        };

    }, [timeLeft]);


    const validationLogin = Yup.object().shape({
        otp: validation.otp('کلمه عبور'),

    })
    const sendPass = (values) => {

        values = {...values, ...addValues};
        setLoading(true)
        Request.init(url, async function (data) {

            setLoading(false)

            if (url === Login) {
                await Cookies.set('Token', data.data.jwtToken, { expires: 7 })
                await Cookies.set('RefreshToken', data.data.refreshToken, { expires: 7 })
                await navigate('/')
            } else if (url === ChangeForgottenPassword) {
                setStep(1)

            }
            // } else {
            //     show('error', data.data.message)
            //
            //
            // }


        }, function (error) {

            show('error', 'رمز وارد شده اشتباه است!')

            setLoading(false)

        }).loginPages().setRouter(navigate).setMainData(values).callRequest()
    };


    const formik = useFormik({
        initialValues,
        onSubmit: sendPass,
        validationSchema: validationLogin,
        validateOnMount: true,
        enableReinitialize: true
    })


    return (
        <form onSubmit={formik.handleSubmit}>

            <Input placeholder='رمز یکبارمصرف' name={'otp'}
                   formik={formik} type={'tel'}
            />


            <div className="flex items-center justify-between">

                <span
                    onClick={setStep} className="cursor-pointer">
                بازگشت
                </span>
                <Button loading={loadingSendPhone} disabled={disableTimer}
                        onClick={sendPhone}
                        className={'defBtn textBtn   m-0 p-0 align-items-center'}>
                                            <span className='mx-2'>ارسال مجدد {timeLeft > 0 &&
                                                <span>{timeLeft}  </span>}</span>
                </Button>

                <Button loading={loading} type="submit" disabled={!formik.isValid}>ثبت</Button>

            </div>
        </form>
    );
};

export default OtpBox;