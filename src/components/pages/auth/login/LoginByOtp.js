import React, {useState} from 'react';
import Input from "../../../microComponents/common/uikits/Input";
import {useFormik} from "formik";
import * as Yup from "yup";
import {validation} from "../../../helper/validation/Validation";
import {useNavigate} from "react-router-dom";
import Button from "../../../microComponents/common/uikits/Button";
import Request from "../../../helper/request/Request";
import show from "../../../helper/Notify/Notify";
import OtpBox from "../OtpBox";
import {GenerateSMSOTP} from "../../../helper/apiList/apiListBusiness";


let initialValues = {
    MobileNumber: '',

}


const LoginByOtp = () => {
    let [step, setStep] = useState(1)
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);


    const validationLogin = Yup.object().shape({
        MobileNumber: validation.mobile('شماره موبایل')

    })


    const handleGenerateSMSOTP = (values) => {
        setLoading(true)

        Request.init(GenerateSMSOTP, async function (data) {
            setLoading(false)


            if (data.data.messageID === 0) {
                setStep(2)
            } else {

                show('error', data.data.message)


            }


        }, function (error) {
            setLoading(false)
            show('error', 'نام کاربری یا کلمه عبور اشتباه است!')
        }).loginPages().setRouter(navigate).setMainData(values).callRequest()


    };


    const formik = useFormik({
        initialValues: initialValues,
        onSubmit: handleGenerateSMSOTP,
        validationSchema: validationLogin,
        validateOnMount: true,
        enableReinitialize: true
    })
    return (
        <div className={'p-3'}>
            {
                step === 1 ?
                    <form onSubmit={formik.handleSubmit}>

                        <Input formik={formik} name={'MobileNumber'} type={'tel'}
                               placeholder={'شماره موبایل'}/>


                        <div className="flex items-center justify-end">


                            <Button loading={loading} type="submit" disabled={!formik.isValid}>ثبت</Button>

                        </div>
                    </form>
                    :
                    <OtpBox setStep={() => setStep(1)} addValues={formik.values}
                            sendPhone={formik.handleSubmit}
                            loadingSendPhone={loading}/>
            } </div>)
}


export default LoginByOtp



