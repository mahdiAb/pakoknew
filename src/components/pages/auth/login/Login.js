import React from 'react';
import LoginByUserName from "./LoginByUserName";
import LoginByOtp from "./LoginByOtp";
import TabPanelCustom from "../../../microComponents/common/TabPanel/TabPanelCustom";

const Login = () => {


    const tabs = [{
        id: 0, text: `ورود با نام کاربری`, content: <LoginByUserName/>,
    }, {
        id: 1, text: `ورود با رمز یکبارمصرف `, content: <LoginByOtp/>,
    },];

    return (<div className='flex w-full h-screen '>
        <div className=' max-w-sm w-full mx-auto mt-20 '>
            <TabPanelCustom tabs={tabs} className={'loginTabs'}/>
        </div>
    </div>);
};

export default Login;