import React, {useState} from 'react';
import Input from "../../../microComponents/common/uikits/Input";
import {useFormik} from "formik";
import * as Yup from "yup";
import {validation} from "../../../helper/validation/Validation";
import {useNavigate} from "react-router-dom";
import Button from "../../../microComponents/common/uikits/Button";
import Request from "../../../helper/request/Request";
import Cookies from 'js-cookie'
import show from "../../../helper/Notify/Notify";
import {ChangeForgottenPassword, GenerateSMSOTP, Login} from "../../../helper/apiList/apiListBusiness";
import OtpBox from "../OtpBox";


let initialValues = {
    MobileNumber: '',
    password: '',
    ConfirmNewPassword: '',
}

let initialValues2 = {
    UserName: '',
    password: '',

}
const LoginByUserName = () => {
    let [step, setStep] = useState(1)
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);


    const validationLogin = Yup.object().shape({
        password: validation.string('کلمه عبور'), UserName: validation.mobile('شماره موبایل')

    })
    const validationConfirm = Yup.object().shape({

        MobileNumber: validation.mobile('شماره موبایل'),
        password: validation.string('کلمه عبور'),
        ConfirmNewPassword: Yup.string().oneOf([Yup.ref('password'), null], 'کلمه عبور و تکرار کلمه عبور با هم برابر نیستند!')


    })


    const handleLogin = (values) => {
        setLoading(true)

        Request.init(Login, async function (res) {
            setLoading(false)

            await Cookies.set('Token', res.data.jwtToken, {expires: 7})
            await Cookies.set('RefreshToken', res.data.refreshToken, {expires: 7})


            await navigate('/')


        }, function (error) {
            setLoading(false)
            show('error', 'نام کاربری یا کلمه عبور اشتباه است!')
        }).loginPages().setRouter(navigate).setMainData(values).callRequest()


    };
    const handleConfirmPass = (values) => {
        setLoading(true)

        Request.init(GenerateSMSOTP, async function (data) {
            setLoading(false)


            if (data.data.messageID === 0) {
                setStep(3)
            } else {
                show('error', data.data.message)


            }


        }, function (error) {
            setLoading(false)

        }).loginPages().setRouter(navigate).setMainData(values).callRequest()


    };


    const formik = useFormik({
        initialValues: step === 1 ? initialValues2 : initialValues,
        onSubmit: step === 1 ? handleLogin : handleConfirmPass,
        validationSchema: step === 1 ? validationLogin : validationConfirm,
        validateOnMount: true,
        enableReinitialize: true
    })


    return (
        <div className={'p-3'}>
            {
                step !== 3 ?
                    <form onSubmit={formik.handleSubmit} className={'m-auto'}>

                        <Input formik={formik} className='max-w-full my-10' name={step === 1 ? 'UserName' : 'MobileNumber'} type={'tel'}
                               placeholder={'شماره موبایل'}/>
                        <Input formik={formik} className='max-w-full my-10' name={'password'} placeholder={'کلمه عبور'} type={'password'}/>
                        {step === 2 &&
                            <Input formik={formik} className='max-w-full my-10' name={'ConfirmNewPassword'} placeholder={'تکرار کلمه عبور'}
                                   type={'password'}/>
                        }


                        <div className="flex items-center justify-between">

                <span
                    onClick={() => setStep(step === 1 ? 2 : 1)} className="cursor-pointer">
                    {step === 1 ? "فراموشی رمز عبور" : 'ورود با نام کاربری'}
                </span>


                            <Button loading={loading} type="submit" disabled={!formik.isValid}>ثبت</Button>

                        </div>
                    </form>
                    :
                    <OtpBox url={ChangeForgottenPassword} setStep={() => setStep(2)} addValues={formik.values}
                            sendPhone={formik.handleSubmit}
                            loadingSendPhone={loading}/>
            } </div>)
}

export default LoginByUserName
