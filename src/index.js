import React from 'react';
import ReactDOM from 'react-dom/client';
import {loadMessages, locale} from "devextreme/localization"
import fa from './components/helper/fa.json'
import config from "devextreme/core/config"
import 'devextreme/dist/css/dx.light.css'
import './styles/FarsiNumerals/fontiran.css'
import './styles/FarsiNumerals/style.css'
import {Provider} from "react-redux";
import store from "./components/redux/store/store";
import 'react-loading-skeleton/dist/skeleton.css'
import './styles/formItems.css'
import './styles/servicesTabs.css'
import './styles/sidebar.css'
import './styles/header.css'
import './styles/globals.css'
import {createBrowserRouter, RouterProvider,} from "react-router-dom";
import Main from "./components/pages/services/Main";
import Login from "./components/pages/auth/login/Login";
import Register from "./components/pages/auth/register/Register";
import BusinessList from "./components/pages/Business/BusinessList";
import {QueryClient, QueryClientProvider} from "react-query";

config({rtlEnabled: true,})
locale("fa-IR-u-ca-persian-nu-latn");
loadMessages(fa);


const router = createBrowserRouter([
    {
        path: "/",
        element: <BusinessList/>,
    },
    {
        path: "/business/:id",
        element: <Main/>,
    },
    {
        path: "/login",
        element: <Login/>,
    },
    {
        path: "/register",
        element: <Register/>,
    },


]);
const queryClient = new QueryClient();
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Provider store={store}><QueryClientProvider client={queryClient}>
        <RouterProvider router={router}/>

    </QueryClientProvider>
    </Provider>
);
