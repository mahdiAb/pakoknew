module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
   ],
  theme: {
    screens: {
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1360px',
    },
    colors: {
      'blue':  "var(--blue)",
      'blue-300':  "var(--blue-300)",
      'gray-300':  "var(--gray-300)",
      'white':  "var(--white)",
      'red':  "var(--red)",
      'black':  "var(--black)",
      'black-200':  "var(--black-200)",


    },

    extend: {
      zIndex: {
        '100': '100',
      },
      dropShadow: {
        'xl': 'var(--shadow)',

      },
      spacing: {
        '128': '32rem',
        '144': '36rem',
      },
      lineHeight: {
        'extra-loose': '3.5',

      },


      borderRadius: {
        'sm': 'var(--rounded-sm)',
        'md': 'var(--rounded)',
        'xl': 'var(--rounded-md)',
        '2xl': 'var(--rounded-lg)',
        'full': 'var(--rounded-full)',
        '0': 'var(--rounded-none)'








      }
    }
  }
}